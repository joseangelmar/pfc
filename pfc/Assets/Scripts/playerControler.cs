using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControler : MonoBehaviour
{
    public float maxSpeed = 5f;
    public float speed = 15f;
    public float jumpForce = 5f;
    public bool isGrounded;
    private Rigidbody2D rb2d;
    private bool keyplus;
    private int rotationAngle=90;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 0.6f, LayerMask.GetMask("floor"));
        if ((hit.collider != null) && (hit.collider.CompareTag("floor")))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        float h = Input.GetAxis("Horizontal");
        if ((h == 0) && (isGrounded == true) && (keyplus == false))
        {
            rb2d.velocity = Vector2.zero;
        }
        else
        {
            rb2d.AddForce(Vector2.right * speed * h);

            float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
            rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);
        }
        if (Input.anyKey)
        {
            keyplus = true;
        }
        else
        {
            keyplus = false;

        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded == true)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
                isGrounded = false;
                
            }
        }
    }
}
